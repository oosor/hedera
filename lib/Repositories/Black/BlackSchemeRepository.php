<?php
/**
 * @package   Hedera
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.07.16
 * @link      https://fabrika-klientov.ua
 * */

namespace Hedera\Repositories\Black;

use GraphAware\Neo4j\OGM\Repository\BaseRepository;

class BlackSchemeRepository extends BaseRepository
{

}
